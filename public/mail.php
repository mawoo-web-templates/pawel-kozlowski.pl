<?php
if ($_SERVER["REQUEST_METHOD"] == "POST") {
    function test_input($data)
    {
        $data = trim($data);
        $data = stripslashes($data);
        $data = htmlspecialchars($data);

        return $data;
    }

    $name          = test_input($_POST["name"]);
    $email         = test_input($_POST["email"]);
    $message       = test_input($_POST["message"]);
    $email_to = "kontakt@pawel-kozlowski.pl";
    $email_subject = "Wiadomosc ze strony";

    $email_message = "Tresc wiadomosci:<br/><br/>";
    $email_message .= "Name: " . $name . "<br/>";
    $email_message .= "Email: " . $email . "<br/>";
    $email_message .= "Message: " . nl2br($message) . "<br/>";

    $headers = "Content-Type: text/html; charset=utf-8" . 'From: ' . $email . "\r\n" . 'Reply-To: ' . $email . "\r\n" . 'X-Mailer: PHP/' . phpversion();

    @mail($email_to, "=?UTF-8?B?" . base64_encode($email_subject) . "?=", $email_message, $headers);

    echo 'success';
}
?>