// JS
import "./assets/js/base";
import "./assets/js/menu";
import "./assets/js/header";
import "./assets/js/animated-scroll-to-top-button";
import "./assets/js/heading-auto-height";
import "./assets/js/copyrights-date";
import "./assets/js/contact-form";

// Custom SCSS
import "./assets/css/styles.scss";

// Images
import "./assets/img/preloader.svg";
import "./assets/img/background.jpg";
import "./assets/img/white-black-hat-seo-pozycjonowanie-stron-www.jpg";
import "./assets/img/components/about/pawel-kozlowski-seo-pozycjonowanie-stron-wroclaw.jpg";
