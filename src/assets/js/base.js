$(document).ready(function() {
  function animateNumbers() {
    var e = !0;
    $(document).on("scroll", function() {
      $(document).scrollTop() + (8 * $(window).height()) / 10 >
        $(".numbers").offset().top &&
        1 == e &&
        ($(".numbers__item-count").each(function() {
          $(this)
            .prop("Counter", 0)
            .animate(
              { Counter: $(this).text() },
              {
                duration: 2e3,
                easing: "swing",
                step: function(e) {
                  $(this).text(Math.ceil(e));
                }
              }
            );
        }),
        (e = !1));
    });
  }

  animateNumbers();
});

$(window).on("load", function() {
  $("#page-loader >div")
    .fadeOut("slow")
    .parent()
    .delay(500)
    .fadeOut();
});
