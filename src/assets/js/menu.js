$(document).ready(function() {
  var button = $("#menu-toggle");
  var menuLink = $(".go-to-href");

  function toggleMenu(t) {
    t.parent().toggleClass("active");
  }

  function animateToSection(t, translation) {
    var id = t.attr("href");

    $("html,body").animate(
      { scrollTop: $(id).offset().top - translation },
      1000,
      "easeInOutExpo",
      function() {
        $(this).clearQueue();
      }
    );
  }

  function matchMedia() {
    return window.matchMedia("(min-width: 992px)").matches;
  }

  button.on("click", function() {
    if (!matchMedia()) {
      toggleMenu($(this));
    }
  });

  menuLink.on("click", function(event) {
    event.preventDefault();

    if (!matchMedia()) {
      toggleMenu(button);
      animateToSection($(this), 60);
    } else {
      animateToSection($(this), 0);
    }
  });
});
