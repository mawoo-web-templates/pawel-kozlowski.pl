$(document).ready(function() {
  var field;
  var form = {
    init: function() {
      field = [
        {
          id: "name",
          required: true,
          regexp: new RegExp("^[a-zA-ZąćęłńóśźżĄĆĘŁŃÓŚŹŻ ]+$")
        },
        {
          id: "email",
          required: true,
          regexp: new RegExp("^[0-9a-z_.-]+@[0-9a-z.-]+\\.[a-z]{2,3}$")
        },
        {
          id: "message",
          required: true,
          regexp: new RegExp(".")
        }
      ];
    },
    removeNotice: function(el) {
      if (arguments.length > 0) {
        if (el.next().hasClass("contact__form-notice")) {
          el.next().remove();
        }
      } else {
        $(".contact__form-notice").remove();
      }
    },
    validation: function(formObj, field) {
      var noErrors = true,
        patternStatus;

      this.removeNotice();

      function checkFieldEmpty(id, regexp) {
        patternStatus = regexp.test($("#" + id).val());

        if ($("#" + id).val() === "") {
          $("#" + id).after(
            '<span class="contact__form-notice">Pole jest wymagane.</span>'
          );

          if (noErrors) {
            noErrors = patternStatus;
          }
        } else {
          checkFieldPattern(id, regexp);
        }
      }

      function checkFieldPattern(id, regexp) {
        patternStatus = regexp.test($("#" + id).val());

        if ($("#" + id).val() !== "" && !patternStatus) {
          $("#" + id).after(
            '<span class="contact__form-notice">Wymagana poprawna wartość pola.</span>'
          );

          if (noErrors) {
            noErrors = patternStatus;
          }
        }
      }

      for (var i = 0; i < field.length; i++) {
        if (field[i].required) {
          checkFieldEmpty(field[i].id, field[i].regexp);
        } else if (!field[i].required) {
          checkFieldPattern(field[i].id, field[i].regexp);
        }
      }

      if (noErrors) {
        this.xhr(formObj);
      } else {
        $("html,body").animate(
          {
            scrollTop:
              $(".contact__form-notice")
                .eq(0)
                .prev()
                .offset().top - 75
          },
          500,
          "easeInOutExpo",
          function() {
            $(this).clearQueue();
          }
        );
      }
    },
    xhr: function(formObj) {
      $.ajax({
        url: "/mail.php",
        type: "POST",
        data: formObj.serialize()
      })
        .done(function(response) {
          form.xhrstatus(response, formObj);
        })
        .fail(function(response) {
          form.xhrstatus(response);
        });
    },
    xhrstatus: function(msg, formObj) {
      if (msg === "success") {
        $("#notice")
          .text("Twoja wiadomość została wysłana pomyślnie. Dziękujemy!")
          .show();

        setTimeout(function() {
          $("#notice").hide();
        }, 5000);

        formObj[0].reset();
      } else if (msg === "fail") {
        $("#notice").text(
          "Twoja wiadomość nie została wysłana. Prosimy spróbować później."
        );
      }
    }
  };

  form.init();

  $("body").on("submit", "#contact-form", function(e) {
    e.preventDefault();
    form.validation($(this), field);
  });

  $("#name, #email, #message").on("focus", function(e) {
    e.preventDefault();
    form.removeNotice($(this));
  });
});
