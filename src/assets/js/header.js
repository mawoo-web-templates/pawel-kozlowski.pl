$(document).ready(function() {
  var timeout;

  function getMenuHeight() {
    return $("#menu").height();
  }
  function getWindowHeight() {
    return $(window).height();
  }

  function calcAndSetHeaderBackgroundHeight() {
    var jumbotron = $("#jumbotron");

    jumbotron.css("height", getWindowHeight() - getMenuHeight() + "px");
  }

  calcAndSetHeaderBackgroundHeight();

  $(window).on("resize", function() {
    clearTimeout(timeout);

    timeout = setTimeout(function() {
      calcAndSetHeaderBackgroundHeight();
    }, 66);
  });
});
