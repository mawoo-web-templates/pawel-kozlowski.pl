$(document).ready(function() {
  var mql = function() {
    return window.matchMedia("(min-width: 768px)").matches;
  };
  var timeout;

  function calcHeight(widthValue) {
    if (arguments.length === 0) {
      $(".js-heading-auto-height").each(function() {
        var headings = $(this).find("h3");
        var maxHeight = 0;

        headings.each(function() {
          if (maxHeight < $(this).outerHeight()) {
            maxHeight = $(this).outerHeight();
          }
        });

        headings.css("height", maxHeight + "px");
      });
    } else {
      $(".js-heading-auto-height").each(function() {
        var headings = $(this).find("h3");

        headings.css("height", widthValue);
      });
    }
  }

  function init() {
    if (mql()) {
      calcHeight();
    } else {
      calcHeight("auto");
    }
  }

  init();

  $(window).on("resize", function() {
    clearTimeout(timeout);

    timeout = setTimeout(function() {
      init();
    }, 66);
  });
});
