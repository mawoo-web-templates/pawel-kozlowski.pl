const path = require("path");
const ExtractTextPlugin = require("extract-text-webpack-plugin"); // ExtractTextPlugin
const HtmlWebpackPlugin = require("html-webpack-plugin"); // HtmlWebpackPlugin
const UglifyJsPlugin = require("uglifyjs-webpack-plugin"); // UglifyjsWebpackPlugin
const CopyWebpackPlugin = require("copy-webpack-plugin"); // CopyWebpackPlugin
const CleanWebpackPlugin = require("clean-webpack-plugin"); // CleanWebpackPlugin

module.exports = {
  entry: "./src/index.js",
  output: {
    filename: "js/bundle.js",
    path: path.resolve(__dirname, "dist")
  },
  module: {
    rules: [
      {
        test: /\.scss$/,
        use: ExtractTextPlugin.extract({
          use: [
            {
              loader: "css-loader",
              options: {
                minimize: true,
                sourceMap: true,
                url: false
              }
            },
            {
              loader: "postcss-loader",
              options: {
                sourceMap: true,
                plugins: function() {
                  return [require("precss"), require("autoprefixer")];
                }
              }
            },
            {
              loader: "sass-loader",
              options: {
                sourceMap: true
              }
            }
          ],
          // use style-loader in development
          fallback: "style-loader"
        })
      },
      {
        test: /\.(gif|png|jpe?g|svg)$/i,
        use: [
          {
            loader: "file-loader",
            options: {
              name: "[name].[ext]",
              outputPath: "img/"
            }
          },
          {
            loader: "image-webpack-loader",
            options: {
              bypassOnDebug: true
            }
          }
        ]
      }
    ]
  },
  plugins: [
    new CleanWebpackPlugin(["dist"]),
    new ExtractTextPlugin({
      filename: "css/[name].[contenthash:8].css"
    }),
    new HtmlWebpackPlugin({
      filename: "index.html",
      template: "./public/index.html",
      minify: {
        collapseWhitespace: true,
        minifyCSS: true,
        minifyJS: true,
        removeComments: true
      }
    }),
    new UglifyJsPlugin({
      sourceMap: true
    }),
    new CopyWebpackPlugin([
      {
        from: "./public/.htaccess"
      },
      {
        from: "./public/mail.php"
      }
    ])
  ],
  devServer: {
    contentBase: path.join(__dirname, "dist"),
    compress: true,
    port: 9000,
    open: true
  },
  devtool: "source-map",
  watch: true
};
